This project contains a SoapUI test project which purpose is to test the conformance of
the XDS Document Repository actor in the context of the EPR project.

This SoapUI project has to be imported within Gazelle Webservice Tester to be executed by the SUT Operator.

# Test environment

The test environment for this project is as follows:
*![Testing environment for XDS Document Repository](./docs/sut_rep_testing_environment.png)

where

* XDS Document Registry actor is played by XDS Registry mock which forwards received requests to XDS Toolkit
* XDS Document Consumer and XDS Document Source actors are played by a SoapUI script executed from Gazelle webservice Tester

# Testing principle

XDS Document Repository is expected to support three transactions:

* ITI-41: Provide and Registry document set, as a responder
* ITI-42: Registry document set, as a initiator. Usually, this transaction is automatically triggered by the receipt of a ITI-41 request
* ITI-43: Retrieve document, as a responder

## Testing of ITI-41 and ITI-42 as a whole

* GWT to send ITI-41 requests and verifies using assertion the content of the response (no soap fault, expected response)
* GWT to send ITI-18 queries to the registry to verify the outcome of the ITI-42 transaction.
* XDSRegistry mock to store the content of the ITI-42 for verification using EVSClient (!!! Repository is supposed to forward the metadata received in ITI-41 after adding some data, that means that we must make sure that the ITI-41 messages we send complies with EPR requirements)

## Testing of ITI-43

We do not want to correlate the testing of ITI-41 + ITI-42 and ITI-43. Two test suites are thus created in GWT. We cannot assume that the first one (ITI-41/ITI-42) succeeded as a pre-requisite for testing ITI-43.
To test the retrieval, we need to know what to retrieve. For each test case, we will
1. Publish a document to the repository
2. Verify we can a “Success” answer
3. Query the registry to get details on the document we have just created
4. Retrieve the document from the repository and compare it with the one we have published (compare hash)

# Content of this project

* _docs_ contains the sequence diagram and any useful documentation for this project
* _soapui_ contains the SoapUI test project
* _submissions_ contains the files which are being submitted to the Document Repository

# Properties

The SoapUI project uses some properties:

* _SUT\_XDSRepository\_PnR_ is the endpoint of the SUT for the ITI-41 transaction: When running the project in GWT, it has to be configured to match 
the SUT endpoint;
* _Patient\_id_ is the identifier of the patient known by the registry;
* CTS\_XDSRegistry_ is the XDS Registry within the CTS to which the XDS Document Repository will forward the metadata;
* RepositoryUniqueId shall match the one assigned to the SUT; it is used to verify the location of the files. When running the project in GWT, it has to be configured to match 
the OID assigned to the SUT;
* SUT\_XDSRepository\_Retrieve is the endpoint of the SUT for the ITI-43 transaction: When running the project in GWT, it has to be configured to match 
 the SUT endpoint.

# Importing the project in SoapUI for changes

eHealthSuisse testing environment is configured to support TLS with mutual authentication. That is to say that the client shall have a certificate to 
establish a SSL connection.
When you import the project in SoapUI, you are asked to give the path to this file. Use keystore 35.jks from EPD project.
Verifies that the SSL Keystore TestRequest property is set to this keystore for each test step within your project.
